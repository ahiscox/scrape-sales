# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/topics/item-pipeline.html

import re


def add_tag(currentTags, newTag):
    return ' '.join(currentTags.split() + newTag.split())


class FilterUnreadPipeline(object):
    """
    Strips out items that we have already read by determining
    the last read items for each site.
    """
    
    def process_item(self, item, spider):
        return item


class PricePipeline(object):
    """ Extracts price from title or description """
    
    def process_item(self, item, spider):
        containsPrice   =   re.compile('[\$]((\d|\,)+(?:\.\d{1,2})?)')
        matchTitle      =   containsPrice.search(item['title'])
        
        price = None
        if matchTitle:
            price   =   matchTitle.group(1)
        else:
            matchDescr  =   containsPrice.search(item['descr'])
            if matchDescr:
                price = matchDescr.group(1)
        
        if price:
            price   =   price.replace('$', '')
            price   =   price.replace(',', '')
            
        item['price']   =   price
        # item['tags']    =   add_tag(item['tags'], 'price')
        
        return item
    
    
class DjangoModelPipeline(object):
    """ Saves the data in a database using django ORM """
    
    def process_item(self, item, spider):
        return item
    
    
class TagPipeline(object):
    """ Go through items and tag when appropriate """
    
    def process_item(self, item, spider):
        return item
    