from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector, XmlXPathSelector
from scrapy.contrib.loader import XPathItemLoader

from app.items import ClassifiedItem

from djapp.classifieds.models import Listing, Site
from django.db.utils import IntegrityError

from urlparse import urlparse
from datetime import datetime


class KijijiSpider(BaseSpider):
    name = "kijiji"
    allowed_domains = ["grandeprairie.kijiji.ca", "peaceriver.kijiji.ca"]
    start_urls = [
	"http://peaceriver.kijiji.ca/f-SearchAdRss?CatId=0&Location=1700303",
	"http://grandeprairie.kijiji.ca/f-SearchAdRss?CatId=0&Location=1700233",
    ]

    def parse(self, response):

        # Setup item
    	domain     =       urlparse(response.url).netloc
       
       	hxx = XmlXPathSelector(response)
    	
    	items = []
    	for i in hxx.select('//item'):
            item        =   ClassifiedItem(site=domain)
            item['uuid']   =   i.select('guid/text()').extract()[0]
            item['title']  =   i.select('title/text()').extract()[0]
            item['descr']  =   i.select('description/text()').extract()[0]
            item['url']    =   i.select('link/text()').extract()[0]
            items.append(item)
            
    	return items
