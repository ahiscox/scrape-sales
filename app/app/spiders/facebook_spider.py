from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector, XmlXPathSelector
from scrapy.contrib.loader import XPathItemLoader

from app.items import ClassifiedItem
from app.facebook import GraphAPI

import json

from djapp.classifieds.models import Listing, Site
from django.db.utils import IntegrityError

from urlparse import urlparse
from datetime import datetime

FACEBOOK_GROUPS = {
                   '204756699575870':   'Peace River Buy or Sell',
                   
}

class FacebookSpider(BaseSpider):
    name = "facebook"
    
    allowed_domains = ['facebook.com',]
    
    accessToken = raw_input("Enter Facebook Access Token: ")
    if not accessToken:
        raise Exception('Access Token Required!')
    
    start_urls = ["https://graph.facebook.com/%s/feed?access_token=%s" % (i[0], accessToken) 
                       for i in FACEBOOK_GROUPS.iteritems() ]
    
    def parse(self, response):
        
        data = json.loads(response.body)['data']
        group = data[0]['id'].split('_')[0]
        
    	items = []
    	for i in data:
            item        =   ClassifiedItem(site='%s' % FACEBOOK_GROUPS[group])
            item['uuid']    =   i['id']
            item['title']   =   i['message']
            # item['url']     =   i['']
            items.append(item)
            
    	return items
