from django.conf.urls import patterns, include, url
from django.views.generic.simple import direct_to_template

from djapp.classifieds.models import Listing
allListings = Listing.objects.all()

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'djapp.views.home', name='home'),
    # url(r'^djapp/', include('djapp.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^$', direct_to_template, {'template': 'classifieds/index.html', 'extra_context': {'listings': allListings}}),


)
