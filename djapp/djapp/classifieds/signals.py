from django.db.models.signals import pre_save
from django.dispatch import receiver

from models import Listing


@receiver(pre_save, sender=Listing)
def extract_data(sender, **kwargs):
	"""Extract useful data from title/description of listings and append it to model"""
	instance 	= 	kwargs['instance']
	created 	=	kwargs['created']

	if created:
		# Run filters to extract info and set tags
		instance.clean_extra_crap() 	# Make sure this is first.
		instance.extract_price()
	#	instance.extract_location()
	#	instance.extract_model()
	#	instance.extract_make()
	#	instance.extract_engine_size()
	#	instance.extract_year()
	#	instance.extract_tags() 	# Make sure this is last.
