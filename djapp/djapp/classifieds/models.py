from django.db import models
import re
from nltk.corpus import stopwords

# Load signals:
# import djapp.classifieds.signals as classifieds_signals


class UselessWords(models.Model):
	word	=	models.CharField(max_length=50)


class Site(models.Model):
	name		=	models.CharField(max_length=50)
	descr		=	models.TextField(blank=True)

class TagCategory(models.Model):
	name	=	models.CharField(max_length=50)
	descr	=	models.TextField(blank=True)


class Tag(models.Model):
	tag_category 	= 	models.ForeignKey(TagCategory)
	name		=	models.CharField(max_length=25)


class ListingManager(models.Manager):

	# note to self: if uuid matches an existing item, check to see if the item has been updated recently

	def create_listing(self, site, uuid, pubDate):
		"""Create a new listing """
		listing = 	self.create(
					site=site, 
					uuid="%s:%s"%(site, uuid),
					date_posted=pubDate )
		return listing


class Listing(models.Model):
	
	objects		=	ListingManager()

	uuid		=	models.CharField(max_length=255, unique=True)

	title 		= 	models.CharField(max_length=255)
	descr 		= 	models.TextField()
	url 		= 	models.URLField()
	
	price 		= 	models.DecimalField(max_digits=9, decimal_places=2, null=True)

	date_posted	= 	models.DateTimeField()
	date_scraped 	= 	models.DateTimeField(auto_now_add=True)
	
	site		=	models.ForeignKey(Site)
	tags		=	models.ManyToManyField(Tag)

	location	=	models.CharField(max_length=255, blank=True)
	email		=	models.EmailField(max_length=254, blank=True)
	phone		=	models.IntegerField(null=True)


	def save(self, *args, **kwargs):
		self.extract_price()

		super(Listing, self).save(*args, **kwargs)


	def clean_extra_crap(self):
		"""remove common words and crap we definitely don't want from description"""
		
		# First remove common stop words
		stpWords = set(stopwords.words('english'))
		cleaned	= filter(lambda w: not stpWords, self.descr.split())
		print cleaned

		commonWords = UselessWords.objects.all()
		for word in commonWords:
			self.descr = self.descr.replace(word, '')

	def extract_price(self):
		"""extract price and append to model"""
		containsPrice 	= 	re.compile('[\$]((\d|\,)+(?:\.\d{1,2})?)')
		matchTitle	=	containsPrice.search(self.title)

		price = None
		if matchTitle:
			price =	matchTitle.group(1)
		else:	
			matchDescr = containsPrice.search(self.descr)
			if matchDescr:
				price = matchDescr.group(1)
		
		if price:
			price 	= 	price.replace('$','')
			price	=	price.replace(',','')
	
		self.price = price
		

	def extract_location(self):
		"""extract location and append to model"""
		pass

	def extract_email(self):
		"""extract email and append to model"""
		pass

	def extract_phone(self):
		"""extract phone number and append to model"""
		pass

	def extract_model(self):
		"""extract vehicle model"""
		pass
	def extract_engine_size(self):
		"""attempt to extract engine size"""
		pass
	def extract_make(self):
		"""extract make"""
		pass
	def extract_year(self):
		"""extract year from model"""
		pass

	def extract_tags(self):
		"""generate tags from model"""
		pass



from django.db.models.signals import pre_save
from django.dispatch import receiver

#@receiver(pre_save, sender=Listing)
#def extract_data(sender, **kwargs):
        #"""Extract useful data from title/description of listings and append it to model"""
        #instance        =       kwargs['instance']

        # Run filters to extract info and set tags
        # instance.clean_extra_crap()     # Make sure this is first.
        #instance.extract_price()
	#instance.save()
        #       instance.extract_location()
        #       instance.extract_model()
        #       instance.extract_make()
        #       instance.extract_engine_size()
        #       instance.extract_year()
        #       instance.extract_tags()         # Make sure this is last.
	
