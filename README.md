# Scrape Sales #

Scrape listings posted to Facebook BuySell groups and Kijiji sites. When saving, strip out common words that are unimportant, and look for keywords in the listings to highlight. 

This mostly works, however there was a rather serious bug in the FB API that I ran into, that leads to some posts text not being retrieved. More information can be found at the Stackoverflow Question:
http://stackoverflow.com/questions/9471315/get-the-proper-fql-source-id-when-querying-multiple-source-ids/

Installation
============

Run this in the console:

	virtualenv classified-scraper
	cd classified-scraper
	source bin/activate
	pip install Django
	pip install Scrapy
	git clone git@bitbucket.org:ahiscox/scrape-sales.git .